Rails.application.routes.draw do
  root 'welcome#index'
  get 'pages/contact' => 'high_voltage/pages#show', id: 'contact'
  get 'pages/courses' => 'high_voltage/pages#show', id: 'courses'
  get 'pages/about' => 'high_voltage/pages#show', id: 'about'
  get 'pages/courses/digital_marketing_certification_course_india' => 'high_voltage/pages#show', id: 'digital_marketing_certification_course_india'
  get 'pages/courses/email_marketing_course' => 'high_voltage/pages#show', id: 'email_marketing_course'
  get 'pages/courses/facebook_marketing_certification_course' => 'high_voltage/pages#show', id: 'facebook_marketing_certification_course'
  get 'pages/courses/youtube_marketing_course' => 'high_voltage/pages#show', id: 'youtube_marketing_course'
  get 'pages/courses/affiliate_marketing_course' => 'high_voltage/pages#show', id: 'affiliate_marketing_course'



end
